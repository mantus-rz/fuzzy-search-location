
# Search Box

## Overview

Dear hiring manager,

according to the feature of :

**Search box**
- with auto-suggest + auto-complete feature
- scanning 1M+ tuples (sample sql file in the zip)
- returns 10-15 suggestion data
- returns desired response in < 1000 miliseconds
- User typed: Jkarta. Results: Jakarta, Indonesia
- User typed: ascott jkrta. Results: Ascott Jakarta, Ascott Kuningan Jakarta
- Python Django Server

, since the last 2 days, I've tried to complete the assignment.

As it had been completed, I would be glad if you review the work.

## 1. Video Result

![Postman](postman.png)

You can see the full demo on this [Gooogle Drive](https://drive.google.com/drive/folders/1WnNvz1D9g3L98tP8w6IxKE0cTppZr7XL) link


## 2. Postman Setup

You can also have similiar Postman setup by using exported JSON file that I had already shared [on this link](https://drive.google.com/file/d/1Md-6WnGI8hy1FyMFQ-L5fwmPib3Hpill/view?usp=sharing)


## 3. Source Code

You can also explore fully-working code that I had already shared [on Gitlab](https://gitlab.com/aysaruminhu/fuzzy-search-location)








