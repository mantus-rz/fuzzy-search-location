from django.db import models

class Location(models.Model):
    text1 = models.CharField(primary_key=True, max_length=255, blank=True)
    text2 = models.CharField(max_length=255, blank=True, null=True)
    
    class Meta:
        managed = False
