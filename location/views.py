from rest_framework import generics
from rest_framework import mixins
from rest_framework.views import APIView
from rest_framework.response import Response

from django.shortcuts import render
from django.http import HttpResponse
from .serializers import LocationSerializer
from .models_transient import Location
from .query import QUERY, QUERY_EQUAL, QUERY_SINGLE
from .utils import is_no_vowel_first_5, is_less_vowel_first_10


def index(request):
    return HttpResponse("Render Test")


class LocationEndpoint(generics.ListAPIView):

    def get_serializer_class(self):
        return LocationSerializer

    def update_word_case(self, word):
        word = word.title() if word.islower() else word
        if word[0].islower() and word[1].isupper():
            word = word.lower()
        return word

    def get_query_str(self, filter_place):
        if not filter_place:
            query_str = QUERY_EQUAL.format(
                word='a',
                word_len=1,
            )
            return query_str
        splitted_words = filter_place.split(' ')

        first_word = splitted_words[0]
        if len(splitted_words) > 1:
            query_str = QUERY.format(
                filter_place=filter_place.title(),
                first_word=first_word.title(),
            )
        else:
            first_word = self.update_word_case(first_word)

            if len(set(first_word.lower())) == 1:
                query_str = QUERY_EQUAL.format(
                    word=first_word[0],
                    word_len=1
                )
            elif len(first_word) < 4:
                query_str = QUERY_EQUAL.format(
                    word=first_word[:3],
                    word_len=len(first_word[:3])
                )
            elif len(first_word) < 3 and is_no_vowel_first_5(first_word):
                query_str = QUERY_EQUAL.format(
                    word=first_word[:3],
                    word_len=len(first_word[:3])
                )
            elif len(first_word) < 6 and is_no_vowel_first_5(first_word):
                query_str = QUERY_EQUAL.format(
                    word=first_word[:1],
                    word_len=len(first_word[:1])
                )
            elif len(first_word) < 5 and is_less_vowel_first_10(first_word):
                query_str = QUERY_EQUAL.format(
                    word=first_word[:2],
                    word_len=2
                )
            else:
                query_str = QUERY_SINGLE.format(
                    filter_place=filter_place,
                    first_word=first_word
                )
        return query_str

    def get_queryset(self):
        query = Location.objects.using('db_location').all()
        filter_place = self.request.query_params.get('query', None)
        query_str = self.get_query_str(filter_place)

        query = Location.objects.using('db_location').raw(query_str)
        memoized = {}
        new_row = []
        i = 0
        for row in query:
            if (row.text2 in memoized.keys()):
                continue
            memoized[row.text2] = True
            new_row.append(row)
            i += 1
            if i > 15:
                break
        return new_row

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
