def is_no_vowel_first_5(text):
    i = 0
    vowel_count = 0
    for letter in text:
        if letter.lower() in ('a', 'e', 'i', 'o', 'u'):
            vowel_count += 1
        if vowel_count >= 2:
            return False
        i += 1
        if i > 6:
            break

    return True


def is_less_vowel_first_10(text):
    text = text.lower()

    vowel = set("aeiou")
    vowel_count = 0
    for letter in text:
        if letter.lower() in vowel:
            vowel_count += 1
    if vowel_count/len(text) > 0.35:
        return False
    return True
